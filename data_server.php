<?php
include 'koneksi.php';

$query_server = mysqli_query($koneksi, 'SELECT * FROM tb_server');
$data_server = mysqli_fetch_all($query_server, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Server</title>
</head>
<body>
    <h1>Data Server</h1>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Rak</th>
            <th>Merk</th>
            <th>IP Addres</th>
            <th>Nama Server</th>
            <th>OS</th>
            <th>Pemilik</th>
        </tr>
        <?php foreach ($data_server as $data) : ?>
            <tr>
                <td><?php echo $data['id']; ?></td>
                <td><?php echo $data['rak']; ?></td>
                <td><?php echo $data['merk']; ?></td>
                <td><?php echo $data['ip_addres']; ?></td>
                <td><?php echo $data['nama_server']; ?></td>
                <td><?php echo $data['os']; ?></td>
                <td><?php echo $data['pemilik']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
