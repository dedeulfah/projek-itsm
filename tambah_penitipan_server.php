<?php 
include 'koneksi.php';
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags --> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    <title>Form Creat Penitipan Server</title>
  </head>
  <body>
 
 <!-- Form Registrasi -->
  <div class="container">
    <h3 class="text-center mt-3 mb-5">SILAHKAN TAMBAH DATA PENITIPAN SERVER</h3>
    <div class="card p-5 mb-5">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label for="nik_nip">NIK/NIP</label>
             <input type="text" class="form-control" id="nik_nip" name="nik_nip" required>
        </div>

        <div class="form-group">
          <label for="nama">nama</label>
          <input type="text" class="form-control" id="nama" name="nama">
        </div>
        <div class="form-group">
          <label for="asal_dinas">asal dinas</label>
          <input type="text" class="form-control" id="asal_dinas" name="asal_dinas">
        </div>

        <div class="form-group">
        <label for="merk_server">Merk Server</label>
        <input type="text" class="form-control" id="merk_server" name="merk_server" required>
        </div>


        <div class="form-group">
        <label for="tgl_penitipan">Tanggal Penitipan</label>
        <input type="text" class="form-control" id="tgl_penitipan" name="tgl_penitipan" required>
        </div>

        <div class="form-group">
        <label for="surat_dinas">Upload PDF Surat Dinas</label>
        <input type="file" class="form-control-file" id="surat_dinas" name="surat_dinas" accept=".pdf" required>
        </div>

        <div class="form-group">
        <label for="lampiran">Upload Lampiran</label>
        <input type="file" class="form-control-file" id="lampiran" name="lampiran" accept=".pdf,.doc,.docx,.png,.jpg,.jpeg" required>
        </div>



        <button type="submit" class="btn btn-primary" name="tambah">Tambah</button>
        <button type="reset" class="btn btn-danger" name="reset">Hapus</button>
      </form>

      <<?php
    if(isset($_POST['tambah'])){
    $nik_nip = $_POST['nik_nip'];
    $nama = $_POST['nama'];
    $asal_dinas = $_POST['asal_dinas'];
    $merk_server = $_POST['merk_server'];
    $tgl_penitipan = $_POST['tgl_penitipan'];

    $pdf_surat_dinas_file = $_FILES['surat_dinas']['name'];
    $pdf_surat_dinas_source = $_FILES['surat_dinas']['tmp_name'];
    $pdf_surat_dinas_folder = './upload/';

    $lampiran_file = $_FILES['lampiran']['name'];
    $lampiran_source = $_FILES['lampiran']['tmp_name'];
    $lampiran_folder = './upload/';

move_uploaded_file($pdf_surat_dinas_source, $pdf_surat_dinas_folder . $pdf_surat_dinas_file);
move_uploaded_file($lampiran_source, $lampiran_folder . $lampiran_file);

$insert = mysqli_query($koneksi, "INSERT INTO l_penitipan_server (nik_nip, nama, asal_dinas,merk_server,tgl_penitipan,surat_dinas,lampiran) VALUES ('$nik_nip', '$nama', '$asal_dinas', '$merk_server', '$tgl_penitipan', '$pdf_surat_dinas_file', '$lampiran_file')");

if ($insert) {
    header("location: permintaan_masuk.php");
} else {
    echo "Maaf, terjadi kesalahan saat mencoba menyimpan data ke database";
}

}
?>

  </div>
  </div>
  

  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
  </body>
</html>