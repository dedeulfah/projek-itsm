<?php 
    session_start();
      if(!isset($_SESSION['login_tb_user_itsm'])) {
        header("location: login.php");
      }else{
?>

<!DOCTYPE html>
<html>
<head>
    <title>pages/domain</title>
<style>
.center-content {
  text-align: center;
}
.centered-list {
  text-align: left;
  display: inline-block;
  padding-left: 0; /* Menghilangkan default padding pada daftar */
}
.centered-list li {
  text-align: left; /* Memastikan teks poin tetap kiri */
  list-style-position: inside; /* Memastikan poin tetap berada di dalam kotak */
}
</style>
</head>
<body>

<div class="center-content">
  <p>PEMBUATAN DOMAIN</p>
  <p>Domain government (go.id) digunakan oleh website resmi pemerintahan pusat atau daerah yang dapat memberikan legalitas dan keresmian dari suatu website pemerintahan.</p>
  <p>Persyaratan :</p>
  <ul class="centered-list">
    <li>Nama domain tidak mengandung kata-kata, karakter atau simbol tertentu</li>
    <li>Nama domain disesuaikan dengan nama aplikasi/proses bisnisnya</li>
    <li>Melampirkan surat pembuatan domain dari SKPD yang mengajukan</li>
  </ul>
  <p><button type="button" class="btn btn-outline-primary" onclick="location.href='tambah_domain.php'">Setuju</button></p>
</div>

</body>
</html>

  <!-- Awal Footer -->
      
  <!-- Akhir Footer -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
<?php } ?>