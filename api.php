<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<label for="nip">Masukan NIP</label>
<input type="number" id="nip">
<button id="searchNIP">Cek</button>
<h1 id="result"></h1>

<table>
    <tr>
        <th>Informasi</th>
        <th>Detail</th>
    </tr>
    <tr>
        <td>Nama : </td>
        <td id="name"></td>
    </tr>
    <tr>
        <td>NIP : </td>
        <td id="NIP"></td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir : </td>
        <td id="ttl"></td>
    </tr>
    <tr>
        <td>Jenis Kelamin : </td>
        <td id="kelamin"></td>
    </tr>
    <tr>
        <td>Gol, Pangkat : </td>
        <td id="golongan"></td>
    </tr>
    <tr>
        <td>Jabatan : </td>
        <td id="jabatan"></td>
    </tr>
    <tr>
        <td>Unit Organisasi : </td>
        <td id="unitorganisasi"></td>
    </tr>
    <tr>
        <td>SKPD : </td>
        <td id="skpd"></td>
    </tr>
    <tr>
        <td>NIK : </td>
        <td id="NIK"></td>
    </tr>
</table>


<script>

let nipForm = document.getElementById('nip')
let btnSearchNip = document.getElementById('searchNIP')
let result = document.getElementById('result')

let NIP = document.getElementById('NIP')
let name = document.getElementById('name')
let ttl = document.getElementById('ttl')
let kelamin = document.getElementById('kelamin')
let golongan = document.getElementById('golongan')
let jabatan = document.getElementById('jabatan')
let unitorganisasi = document.getElementById('unitorganisasi')
let skpd = document.getElementById('skpd')
let NIK = document.getElementById('NIK')

btnSearchNip.addEventListener("click", function () {
    nip = nipForm.value
    fetch(`https://api-simpeg.tasikmalayakab.go.id/pegawai/${nip}`, {
        method: "get",
        headers: {
          "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJZFVzZXIiOiIyIiwiVXNlcm5hbWUiOiJiYXBwZWRhIn0.OMyZAndGTkpEiTzOz3_12skoG7AYjf4rTHpR5IsZnLI",
        }
    })
    .then(response => response.json())
    .then(data => {
        if (data.status == 'error') {
            result.innerHTML = data.message
        } else {
            NIP.innerHTML = data.data.nip_baru
            name.innerHTML = data.data.nama_pegawai
            ttl.innerHTML = `${data.data.tempat_lahir}, ${data.data.tanggal_lahir}`
            kelamin.innerHTML = data.data.gender
            golongan.innerHTML = `${data.data.nama_golongan},${data.data.nama_pangkat}`
            jabatan.innerHTML = data.data.nomenklatur_jabatan
            unitorganisasi.innerHTML = data.data.nama_unor
            skpd.innerHTML = data.data.nomenklatur_pada
            NIK.innerHTML = data.data.nik
        }
    });
});

</script>

</body>
</html>