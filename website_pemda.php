<?php
include 'koneksi.php';

$query_website = mysqli_query($koneksi, 'SELECT * FROM tb_websitepemda');
$data_website = mysqli_fetch_all($query_website, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Website Pemerintah Daerah</title>
</head>
<body>
    <h1>Data Website Pemerintah Daerah</h1>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Domain Name</th>
            <th>IP Public</th>
            <th>Status</th>
        </tr>
        <?php foreach ($data_website as $data) : ?>
            <tr>
                <td><?php echo $data['id']; ?></td>
                <td><?php echo $data['domain_name']; ?></td>
                <td><?php echo $data['ip_public']; ?></td>
                <td><?php echo $data['status']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
