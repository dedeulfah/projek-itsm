
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    <title>ITSM Kabupaten Tasikmalaya</title>
  </head>
  <body>

  <!-- Jumbotron -->
            <div class="container mt-3 ">
            <h1>Detail Kinerja Pegawai | ITSM</h1>

          
       </div> 
      </nav>
  <!-- Akhir Navbar -->

  <!-- Menu -->
    <div class="container mt-5 ">
        
<head>
    <h><b>pages/Kinerja Pegawai</b></h>
  <title>Data pegawai</title>
  <link rel="stylesheet" href="biodata.css">
</head>
<body>
  <div class="card">
    <div class="card-header">
      <img src="image/yulia.png" alt="Your Photo">
    </div>
    <div class="card-body">
    
      <h2>Yulia Oktavia</h2>
      <p><strong>Nik/Nip:</strong> 3036152489</p>
      <p><strong>Email :</strong> Yuliaokta513@gmail.com</p>
      <p><strong>Asal dinas :</strong> Diskominfo</p>
      <p><strong>jabatan:</strong> Back End Develover</p>
      
    </div>
  </div>
</body>


        <table class="table table-striped">
            <thead>
            <h><b>Detail Kinerja Pegawai</h>
                <tr>
                <th scope="col">No.</th>
            <th scope="col">user</th>
            <th scope="col">Asal Dinas</th>
            <th scope="col">waktu mulai</th>
            <th scope="col">waktu selesai</th>
            <th scope="col">uraian kegiatan</th>
          
                </tr>
            </thead>

            <tbody>
    <tr>
        <td>1</td>
        <td>Yulia Oktavia</td>
        <td>Dinas kesehatan</td>
        <td>09:30</td>
        <td>12:00</td>
        <td>Pembuatan Domain</td>
        
    </tr>

    <tr>
        <td>1</td>
        <td>Yulia Oktavia</td>
        <td>Dinas kesehatan</td>
        <td>09:30</td>
        <td>12:00</td>
        <td>Pembuatan Domain</td>
        </table>
    </tr>
    
    
            
    </div>
  <!-- Akhir Menu -->
    

  <!-- Awal Footer -->
      //<hr class="footer">
      <div class="container">
        <div class="row footer-body">
          <div class="col-md-6">
          <div class="copyright">
            <strong></strong> <i class="far fa-copyright"></i> </p>
          </div>
          </div>

          <div class="col-md-6 d-flex justify-content-end">
          
          </div>
        </div>
      </div>
  <!-- Akhir Footer -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script>
      $(document).ready(function() {
          $('#example').DataTable();
      } );
    </script>
  </body>
</html>